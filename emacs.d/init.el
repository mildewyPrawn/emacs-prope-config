;; Quitamos la barra de File, Edit, Options, etc
(menu-bar-mode 0)
;; Quitamos la barra de abrir, guardar, undo, copiar, pegar
(tool-bar-mode 0)

;; Añadimos las líneas a la izquierda
(global-linum-mode 1)

;; Definimos  una combinación  de  teclas para  movernos al  siguiente
;; buffer
(global-set-key (kbd "C-x C-¿") 'next-buffer)
;; Definimos  una  combinación  de  teclas  para  movernos  al  buffer
;; anterior
(global-set-key (kbd "C-x C-'") 'previous-buffer)

;; Definimos una  combinación para  regresar al buffer  anterior, esta
;; función la definimos nosotros.
(global-set-key (kbd "C-x C-o") 'other-window-backward)

;; Función que nosotros creamos para  un propósito específico. En este
;; caso, poder regresar a la ventana anterior.
(defun other-window-backward (&optional n)
  "Select the n-th previous window"
  (interactive "p")
  (other-window (- (prefix-numeric-value n))))

;; De donde vamos a descargar paquetes.
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(package-refresh-contents)
(unless package-archive-contents
  (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;; Haskell-mode
(use-package haskell-mode)

;; un tema oscuro (muy famoso), si quiren uno blanco borren esto
(use-package dracula-theme
  :ensure t
  :init
  (setq dracula-theme t)
  (load-theme 'dracula t))

;; Colorea qué paréntesis cierra con cuál
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Le metemos autocerrar pesitos a latex
(use-package latex-mode
  :ensure nil
  :defer t
  :hook (latex-mode . (lambda () setq electric-pair-pairs
			'(?$ . ?$))))

;; Ayuda para recomendar combinaciones de teclas que nos pueden servir.
(use-package which-key
  :config (which-key-mode))

;; Para ver pdfs mejor, descomentar hasta que se tenga instalado LaTeX
;(use-package pdf-tools
;  :hook (pdf-view-mode . (lambda () (display-line-numbers-mode 0)))
;  :config (pdf-tools-install))

;; autocompletar paréntesis
(electric-pair-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(pdf-tools rainbow-delimiters dracula-theme haskell-mode use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
