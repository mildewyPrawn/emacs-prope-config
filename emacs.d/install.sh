echo "Borrando configuración anterior"
rm -rf $HOME/.emacs.d
rm -rf $HOME/.emacs*
echo "Moviendo nueva configuración"
mkdir $HOME/.emacs.d/
cp init.el $HOME/.emacs.d/
